package com.example.vulkanmprotect;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.vulkanmprotect.databinding.ActivityMainBinding;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    static String TAG = "vulkan_mprotect";

    static {
        System.loadLibrary("vulkan_mprotect");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        com.example.vulkanmprotect.databinding.ActivityMainBinding binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        TextView tv = binding.sampleText;

        int[] results = runAllMProtectTests();
        String message;
        if (results.length > 0) {
            message = "Successful memory indices: " + Arrays.toString(results);
            tv.setTextColor(Color.GREEN);
            Log.i(TAG, message);
        } else {
            message = "No memory index was successful.";
            tv.setTextColor(Color.RED);
            Log.e(TAG, message);
        }
        tv.setText(message);
    }

    public native int[] runAllMProtectTests();
}