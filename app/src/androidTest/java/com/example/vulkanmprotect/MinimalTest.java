package com.example.vulkanmprotect;

import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;


@RunWith(AndroidJUnit4.class)
public class MinimalTest {

    static {
        System.loadLibrary("minimal");
    }

    public native boolean runMinimalMprotect();

    @Test
    public void useAppContext() {
        assertTrue(runMinimalMprotect());
    }
}